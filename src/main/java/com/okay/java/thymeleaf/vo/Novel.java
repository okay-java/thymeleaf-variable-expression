package com.okay.java.thymeleaf.vo;

public class Novel {

	private String title;
	private String published;
	private int pages;
	private double price;
	private String genre;
	private Author author;
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublished() {
		return published;
	}
	public void setPublished(String published) {
		this.published = published;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	public Novel() {
		super();
	}
	public Novel(String title, String published, int pages, double price, String genre, Author author) {
		super();
		this.title = title;
		this.published = published;
		this.pages = pages;
		this.price = price;
		this.genre = genre;
		this.author = author;
	}
	public Author getAuthor() {
		return author;
	}
	public void setAuthor(Author author) {
		this.author = author;
	}
	
	
	
}
