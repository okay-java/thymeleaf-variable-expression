package com.okay.java.thymeleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.okay.java.thymeleaf.vo.Novel;

@Controller
public class IndexController {

	@GetMapping("/")
	public String indexpage() {
		return "index";
	}
	
	@GetMapping("/create-novel")
	public String createNovelLink(Model model) {
		
		model.addAttribute("novelObject", new Novel());
		
		return "add-novel";
	}
}
