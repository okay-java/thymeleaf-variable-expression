package com.okay.java.thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymeleafExpsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThymeleafExpsApplication.class, args);
	}

}
