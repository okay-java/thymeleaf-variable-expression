package com.okay.java.thymeleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.okay.java.thymeleaf.vo.Author;
import com.okay.java.thymeleaf.vo.Novel;

@Controller
public class NovelController {

	@GetMapping("/novel")
	public String novelpage(Model model) {
		
		Author authorObject = new Author("Chetan", "Bhagat", "Mumbai", "India");
		Novel novelObject = new Novel("ONE INDIAN GIRL", "2020-03-31", 280, 550.00, "Fiction", authorObject);
		
		model.addAttribute("novelObject", novelObject);
		
		return "novel";
	}
	
	@PostMapping("/add-novel")
	public String addNovel(@ModelAttribute Novel novel, Model model) {
		System.out.println(" add novel ");
		model.addAttribute("novelObject", new Novel());
		model.addAttribute("message", "Successfully added.");
		return "add-novel";
		
	}
}
