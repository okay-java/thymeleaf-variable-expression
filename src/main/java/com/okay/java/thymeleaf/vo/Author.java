package com.okay.java.thymeleaf.vo;

public class Author {

	private String firstname;
	private String lastname;
	private String city;
	private String country;
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Author(String firstname, String lastname, String city, String country) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.city = city;
		this.country = country;
	}
	public Author() {
		super();
	}
	
	
}
